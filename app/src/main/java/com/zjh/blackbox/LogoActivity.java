package com.zjh.blackbox;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;

import java.util.logging.Handler;
import java.util.logging.LogRecord;

/**
 * @Author: BlackBox
 * @Date: 2021/3/7 17:08
 */
public class LogoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState);
        //加载启动界面
        setContentView(R.layout.logo);
        //设置等待时间，单位为毫秒
        final Integer time = 2000;
        Thread thread = new Thread(){

            @Override
            public void run() {
                super.run();
                try {
                    sleep(time);
                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(intent);
                    finish();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        thread.start();
    }
}
