package com.zjh.blackbox;

import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import com.zjh.blackbox.tetris.Config;
import com.zjh.blackbox.tetris.controller.MainController;
import com.zjh.blackbox.tetris.model.BoxesModel;
import com.zjh.blackbox.tetris.model.MapsModel;

import java.util.concurrent.*;

/**
 * @author 98283
 */
public class MainActivity extends Activity implements View.OnClickListener {

    BoxesModel boxesModel;
    MapsModel mapsModel;
    MainController mainController;

    String string = " ";

    /**
     * x宽高
     * boxSize 盒子大小
     * boxTape 方块的类型
     */
    int boxSize;

    /**
     * 游戏区域控件
     */
    View view;
    View viewNextBox;


    /**
     * 创建线程池
     */
    ExecutorService executorService = new ThreadPoolExecutor(3, 4, 1, TimeUnit.SECONDS, new ArrayBlockingQueue(8));

    Handler handler = new Handler() {
        @Override
        public void handleMessage(@NonNull Message msg) {
            switch (msg.what) {
                case 1:
                    boxesModel.move(0, 1);
                    refresh();
                    break;
                case 2:
                    refresh();
                default:
                    throw new IllegalStateException("Unexpected value: " + msg.what);
            }
        }
    };


    private Point[] boxes;
    private TextView nowScore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initData();
        initListener();
        initView();
        mainController = new MainController();
    }

    /**
     * 初始化数据
     */
    public void initData() {

        int width = getScreenWidth(this);
        Config.mapWeight = width * 3 / 4;
        Config.mapHeight = Config.mapWeight * 3 / 2;

        boxSize = Config.mapWeight / 10;

        //初始化方块
        boxesModel = new BoxesModel();
        boxesModel.setBoxSize(boxSize);
        mapsModel = new MapsModel(boxSize);
        boxesModel.setMapsModel(mapsModel);

    }

    /**
     * 监听按键
     */
    public void initListener() {
        findViewById(R.id.btnUp).setOnClickListener(this);
        findViewById(R.id.btnDown).setOnClickListener(this);
        findViewById(R.id.btnLeft).setOnClickListener(this);
        findViewById(R.id.btnRight).setOnClickListener(this);
        findViewById(R.id.btnStart).setOnClickListener(this);
        findViewById(R.id.btnStop).setOnClickListener(this);
    }

    /**
     * 取得屏幕宽度
     * @param context
     * @return
     */
    public static int getScreenWidth(Context context) {
        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        windowManager.getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics.widthPixels;
    }


    /**
     * 初始化视图
     */
    public void initView() {

        final Paint pausePaint = new Paint();
        pausePaint.setColor(0x8FDDA0DD);
        pausePaint.setAntiAlias(true);
        pausePaint.setTextSize(50);

        final FrameLayout layoutGame = findViewById(R.id.layoutGame);
        view = new View(this) {
            //        重写区域绘制
            @Override
            protected void onDraw(Canvas canvas) {
                super.onDraw(canvas);

                boxesModel.drawBoxes(canvas);
                mapsModel.drawMaps(canvas);
                nowScore.setText(""+Config.nowScore);
                if (mainController.isPause() && !mainController.isGameOver()) {
                    canvas.drawText("暂停", Config.mapWeight / 2 - pausePaint.measureText("暂停") / 2, Config.mapHeight / 2, pausePaint);
                }

                if (mainController.isGameOver()) {
                    canvas.drawText("游戏结束", Config.mapWeight / 2 - pausePaint.measureText("游戏结束") / 2, Config.mapHeight / 2, pausePaint);
                }
            }
        };
        view.setLayoutParams(new FrameLayout.LayoutParams(Config.mapWeight, Config.mapHeight));
        view.setBackgroundColor(0x10000000);
        layoutGame.addView(view);
        nowScore = (TextView) findViewById(R.id.nowScore);

        //实例化下一块预览区域
        viewNextBox = new View(this){
            @Override
            protected void onDraw(Canvas canvas) {
                super.onDraw(canvas);
                boxesModel.drawNext(canvas);
            }
        };
        //设置大小
        viewNextBox.setLayoutParams(new FrameLayout.LayoutParams(100, 100));
        viewNextBox.setBackgroundColor(0x10000000);
        //添加进父容器
        FrameLayout frameLayout = findViewById(R.id.nextBox);
        frameLayout.addView(viewNextBox);
    }

    /**
     * 视图刷新
     */
    public void refresh(){
        view.invalidate();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnRight:
                boxesModel.move(1, 0);
                refresh();
                break;
            case R.id.btnLeft:
                boxesModel.move(-1, 0);
                refresh();
                break;
            case R.id.btnUp:
                boxesModel.rotate();
                refresh();
                break;
            case R.id.btnDown:
                if (mapsModel.moveDown(boxesModel)) {
                    mainController.setGameOver(true);
                }
                viewNextBox.invalidate();
                refresh();
                break;
            case R.id.btnStart:
                startGame();
                refresh();
                break;
            //这里有一个问题，这里刷新view没有效果不知道为什么
            case R.id.btnStop:
                pause();
                refresh();
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + view.getId());
        }
    }


    /**
     * 暂停游戏
     */
    public void pause() {
        mainController.setPause(!mainController.isPause());
    }

    /**
     * 开始游戏
     */
    public void startGame() {

        mainController.setGameOver(false);
        mainController.setPause(false);
        mainController.setPoint(false);
        Config.nowScore = 0;
        mapsModel.cleanMaps();
        boxes = boxesModel.newBoxes();
        view.invalidate();
        viewNextBox.invalidate();
        executorService.execute(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    try {
                        Thread.sleep(750);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    if (!mainController.isPause()) {
                        handler.sendEmptyMessage(1);
                    }
                }

            }
        });
    }

    /**
     * 测试
     */
//    public void test() {
//
//        executorService.execute(new Runnable() {
//            @Override
//            public void run() {
//
//                try {
//                    Thread.sleep(1000);
//                    string = "我是孤单的";
//                    handler.sendEmptyMessage(1);
//                    Thread.sleep(1000);
//                    string = "直到遇见了你";
//                    handler.sendEmptyMessage(1);
//                    Thread.sleep(1000);
//                    string = "从那天起";
//                    handler.sendEmptyMessage(1);
//                    Thread.sleep(1000);
//                    string = "我的早起有人";
//                    handler.sendEmptyMessage(1);
//                    Thread.sleep(1000);
//                    string = "我的睡前也有人";
//                    handler.sendEmptyMessage(1);
//                    Thread.sleep(1000);
//                    string = "后天我们";
//                    handler.sendEmptyMessage(1);
//                    Thread.sleep(1000);
//                    string = "说起了过往";
//                    handler.sendEmptyMessage(1);
//                    Thread.sleep(1000);
//                    string = "我动了心";
//                    handler.sendEmptyMessage(1);
//                    Thread.sleep(1000);
//                    string = "给你唱了歌";
//                    handler.sendEmptyMessage(1);
//                    Thread.sleep(1000);
//                    string = "我没见过你";
//                    handler.sendEmptyMessage(1);
//                    Thread.sleep(1000);
//                    string = "但我发现";
//                    handler.sendEmptyMessage(1);
//                    Thread.sleep(1000);
//                    string = "我爱你";
//                    handler.sendEmptyMessage(1);
//                    Thread.sleep(1000);
//                    string = "并不在意";
//                    handler.sendEmptyMessage(1);
//                    Thread.sleep(1000);
//                    string = "你的外貌";
//                    handler.sendEmptyMessage(1);
//                    Thread.sleep(1000);
//                    string = "富贵与贫穷";
//                    handler.sendEmptyMessage(1);
//                    Thread.sleep(1000);
//                    string = "只在意你";
//                    handler.sendEmptyMessage(1);
//                    Thread.sleep(1000);
//                    string = "很多不懂";
//                    handler.sendEmptyMessage(1);
//                    Thread.sleep(1000);
//                    string = "会认真学";
//                    handler.sendEmptyMessage(1);
//                    Thread.sleep(1000);
//                    string = "认真听";
//                    handler.sendEmptyMessage(1);
//                    Thread.sleep(1000);
//                    string = "到现在";
//                    handler.sendEmptyMessage(1);
//                    Thread.sleep(1000);
//                    string = "都好";
//                    handler.sendEmptyMessage(1);
//                    Thread.sleep(1000);
//                    string = "小乖乖";
//                    handler.sendEmptyMessage(1);
//                    Thread.sleep(1000);
//                    string = "虽然闹腾";
//                    handler.sendEmptyMessage(1);
//                    Thread.sleep(1000);
//                    string = "但可爱";
//                    handler.sendEmptyMessage(1);
//                    Thread.sleep(1000);
//                    string = "虽然容易生气";
//                    handler.sendEmptyMessage(1);
//                    Thread.sleep(1000);
//                    string = "但不是比";
//                    handler.sendEmptyMessage(1);
//                    Thread.sleep(1000);
//                    string = "闷在心里更好";
//                    handler.sendEmptyMessage(1);
//                    Thread.sleep(1000);
//                    string = "还会说，不会闹";
//                    handler.sendEmptyMessage(1);
//                    Thread.sleep(1000);
//                    string = "这么好的女孩子";
//                    handler.sendEmptyMessage(1);
//                    Thread.sleep(1000);
//                    string = "会带我去";
//                    handler.sendEmptyMessage(1);
//                    Thread.sleep(1000);
//                    string = "吃好吃的";
//                    handler.sendEmptyMessage(1);
//                    Thread.sleep(1000);
//                    string = "也温柔";
//                    handler.sendEmptyMessage(1);
//                    Thread.sleep(1000);
//                    string = "这么好的女孩子";
//                    handler.sendEmptyMessage(1);
//                    Thread.sleep(1000);
//                    string = "哪里找";
//                    handler.sendEmptyMessage(1);
//                    Thread.sleep(1000);
//                    string = "反正就是爱你";
//                    handler.sendEmptyMessage(1);
//                    Thread.sleep(1000);
//                    string = "打死不跑";
//                    handler.sendEmptyMessage(1);
//                    Thread.sleep(1000);
//                    string = "现在我不怕";
//                    handler.sendEmptyMessage(1);
//                    Thread.sleep(1000);
//                    string = "好好珍惜你";
//                    handler.sendEmptyMessage(1);
//                    Thread.sleep(2000);
//                    string = "爱你的";
//                    handler.sendEmptyMessage(1);
//                    Thread.sleep(2000);
//                    string = "郑锦鸿";
//                    handler.sendEmptyMessage(1);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//
//            }
//        });
//
//    }
}
