package com.zjh.blackbox.tetris;

/**
 * @author 98283
 */
public class Config {

    /**
     * 地图x方向上的格子数
     */
    public static final int mapX = 10;

    /**
     * 地图y方向上的格子数
     */
    public static final int mapY = 15;

    /**
     * 地图x方向的宽度
     */
    public static int mapWeight;

    /**
     * 地图y方向的高度
     */
    public static int mapHeight;

    /**
     * 最高分数
     */
    public static int maxScore = 0;

    /**
     * 最低分数
     */
    public static int nowScore = 0;
}
