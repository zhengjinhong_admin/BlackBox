package com.zjh.blackbox.tetris.controller;

/**
 * @Author: BlackBox
 * @Date: 2021/2/26 14:27
 */
public class MainController {

    /**
     * isPause 暂停状态
     * isGameOver 游戏结束状态
     * isPoint 分数条件
     */
    boolean isPause, isGameOver, isPoint;
    int maxScore, minScore;

    public int getMaxScore() {
        return maxScore;
    }

    public void setMaxScore(int maxScore) {
        this.maxScore = maxScore;
    }

    public int getMinScore() {
        return minScore;
    }

    public void setMinScore(int minScore) {
        this.minScore = minScore;
    }

    public boolean isPause() {
        return isPause;
    }

    public void setPause(boolean pause) {
        isPause = pause;
    }

    public boolean isGameOver() {
        return isGameOver;
    }

    public void setGameOver(boolean gameOver) {
        isGameOver = gameOver;
    }

    public boolean isPoint() {
        return isPoint;
    }

    public void setPoint(boolean point) {
        isPoint = point;
    }
}
