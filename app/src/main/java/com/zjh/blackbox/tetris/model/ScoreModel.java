package com.zjh.blackbox.tetris.model;

/**
 * @author 98283
 */
public class ScoreModel {

    static int maxScore;
    static int nowScore;

    public ScoreModel(){
        nowScore = 0;
    }

    public int getScore() {
        return nowScore;
    }

    public void setScore(int score) {
        nowScore = score;
    }

    public int getMaxScore() {
        return maxScore;
    }

    public void setMaxScore(int maxScore) {
        if (maxScore > ScoreModel.maxScore) {
            ScoreModel.maxScore = maxScore;
        }
    }

    public void addNowScore(){
        nowScore++;
    }
}
