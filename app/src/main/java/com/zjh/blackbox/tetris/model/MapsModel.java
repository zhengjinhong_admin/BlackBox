package com.zjh.blackbox.tetris.model;

import android.graphics.Canvas;
import android.graphics.Paint;
import com.zjh.blackbox.tetris.Config;

/**
 * @author 98283
 */
public class MapsModel {

    private final ScoreModel scoreModel;
    /**
     * 方块大小
     */
    int boxSize, weight, height;
    /**
     * 地图
     */
    boolean[][] maps;
    /**
     * paint 辅助线画笔TYPE
     * mapPaint 地图画笔
     * pausePaint 暂停画笔
     */
    Paint paint, mapPaint, pausePaint;

    public boolean isGameOver;

    public MapsModel(int boxSize) {

        this.boxSize = boxSize;
        this.weight = Config.mapWeight;
        this.height = Config.mapHeight;

        scoreModel = new ScoreModel();

        //初始化地图
        maps = new boolean[Config.mapX][Config.mapY];

        mapPaint = new Paint();
        mapPaint.setColor(0x8FFFB6C1);
        mapPaint.setAntiAlias(true);

        paint = new Paint();
        paint.setColor(0x20077777);
        paint.setAntiAlias(true);

        pausePaint = new Paint();
        pausePaint.setColor(0x8FDDA0DD);
        pausePaint.setAntiAlias(true);
        pausePaint.setTextSize(100);
    }



    public void drawMaps(Canvas canvas) {

        //
        for (int i = 0; i < maps.length; i++) {
            for (int j = 0; j < maps[i].length; j++) {
                if (maps[i][j]) {
                    canvas.drawRect(i * boxSize, j * boxSize, i * boxSize + boxSize, j * boxSize + boxSize, mapPaint);
                }
            }
        }
        //绘制横线
        for (int i = 0; i < maps.length; i++) {
            canvas.drawLine(i * boxSize, 0, i * boxSize, height, paint);
        }
        //绘制竖线
        for (int j = 0; j < maps[0].length; j++) {
            canvas.drawLine(0, j * boxSize, weight, j * boxSize, paint);

        }

    }

    /**
     * 下落
     * @param boxesModel
     */
    public boolean moveDown(BoxesModel boxesModel) {

        //移动到底部
        while (boxesModel.move(0, 1)) {
        }
        //方块叠加
        for (int i = 0; i < boxesModel.boxes.length; i++) {
            maps[boxesModel.boxes[i].x][boxesModel.boxes[i].y] = true;
        }
        //消行
        cleanLine();
        //创建新的方块
        boxesModel.boxes = boxesModel.boxesText;
        boxesModel.move(3,0);
        return checkOver(boxesModel);
    }

    /**
     * 判断游戏是否结束
     * @param boxesModel
     */
    public boolean checkOver(BoxesModel boxesModel) {
        for (int i = 0; i < boxesModel.boxes.length; i++) {
            if (maps[boxesModel.boxes[i].x][boxesModel.boxes[i].y]) {
                return true;
            }
        }
        return false;
    }

    /**
     * 消行
     *
     * @return
     */
    private boolean cleanLine() {

        for (int y = maps[0].length - 1; y > 0; --y) {
            if (checkLine(y)) {
                deleteLine(y);
                scoreModel.addNowScore();
                Config.nowScore++;
                ++y;
            }
        }
        return true;
    }

    /**
     * 判断是否可消除
     */
    private boolean checkLine(int y) {
        for (int x = 0; x < maps.length - 1; x++) {
            if (!maps[x][y]) {
                return false;
            }
        }
        return true;
    }

    /**
     * 执行消行
     *
     * @param dy
     * @return
     */
    private boolean deleteLine(int dy) {
        System.out.println(dy);
        for (int y = dy; y > 0; --y) {
            for (int x = 0; x < maps.length; x++) {
                maps[x][y] = maps[x][y - 1];
            }
        }
        return true;
    }

    /**
     * 边界判断
     *
     * @param x
     * @param y
     * @return true出界false未出界
     */
    public boolean borderJudgment(int x, int y) {

        return (x < 0 || y < 0 || x >= maps.length || y >= maps[0].length || maps[x][y]);
    }

    /**
     * 清除地图
     */
    public void cleanMaps() {

        //清除地图
        for (int i = 0; i < maps.length; i++) {
            for (int j = 0; j < maps[0].length; j++) {
                maps[i][j] = false;
            }
        }

    }

    public void setBoxSize(int boxSize) {
        this.boxSize = boxSize;
    }

    public int getBoxSize() {
        return boxSize;
    }
}
