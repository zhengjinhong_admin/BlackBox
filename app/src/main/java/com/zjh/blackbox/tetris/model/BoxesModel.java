package com.zjh.blackbox.tetris.model;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;


import java.util.LinkedList;
import java.util.Queue;
import java.util.Random;

/**
 * @author 98283
 */
public class BoxesModel {

    Queue<BoxesModel> queue;
    MapsModel mapsModel;

    /**
     * 方块
     * boxes 当前的
     * nowBoxes 新的方块
     * boxesText 下一块
     */
    Point[] boxes = new Point[]{}, boxesText = new Point[]{}, nowBoxes = new Point[]{};
    /**
     * x宽高
     * boxSize 盒子大小
     * boxTape 方块的类型
     * type 种类别
     */
    int xWright, xHeight, boxSize, boxType, type;
    /**
     * boxPoint 方块画笔
     */
    Paint boxPaint;

    public BoxesModel() {
        boxPaint = new Paint();
        boxPaint.setColor(0x8FFFB6C1);
        boxPaint.setAntiAlias(true);
        queue = new LinkedList<BoxesModel>();
    }




    /**
     * 取得新的方块
     */
    public Point[] newBoxes() {
        boxes = getNowBoxes();
        return boxes;
    }


    /**
     * 方块模型
     * @return 新的方块
     */
    public Point[] getNowBoxes() {
        boxType = type;
        Random random = new Random();
        type = random.nextInt(8);
        switch (type) {
            /**
             * **
             * **
             */
            case 0:
                nowBoxes = new Point[]{new Point(1, 0), new Point(0, 0), new Point(0, 1), new Point(1, 1)};
                break;
            /**
             * *
             * *
             * **
             */
            case 1:
                nowBoxes = new Point[]{new Point(0, 2), new Point(0, 0), new Point(0, 1), new Point(1, 2)};
                break;
            /**
             *  *
             *  *
             * **
             */
            case 2:
                nowBoxes = new Point[]{new Point(1, 2), new Point(1, 0), new Point(1, 1), new Point(0, 2)};
                break;
            /**
             *   *
             *   *
             *   *
             *   *
             */
            case 3:
                nowBoxes = new Point[]{new Point(1, 1), new Point(1, 0), new Point(1, 2), new Point(1, 3)};
                break;
            /**
             *  **
             * **
             */
            case 4:
                nowBoxes = new Point[]{new Point(1, 1), new Point(1, 0), new Point(2, 0), new Point(0, 1)};
                break;
            /**
             * ***
             *  *
             */
            case 5:
                nowBoxes = new Point[]{new Point(1, 0), new Point(1, 1), new Point(0, 0), new Point(2, 0)};
                break;
            /**
             * **
             *  **
             */
            case 6:
                nowBoxes = new Point[]{new Point(1, 0), new Point(0, 0), new Point(1, 1), new Point(2, 1)};
                break;
            /**
             * *
             * ***
             */
            case 7:
                nowBoxes = new Point[]{new Point(0, 1), new Point(0, 0), new Point(1, 1), new Point(2, 1)};
                break;
            /**
             * *
             * *
             * ***
             */
            case 8:
                nowBoxes = new Point[]{new Point(1, 0), new Point(0, 0), new Point(1, 1), new Point(2, 1)};
                break;

            default:
                throw new IllegalStateException("Unexpected value: " + boxType);
        }
        return nowBoxes;
    }

    /**
     * @return 旋转
     */
    public boolean rotate() {
        for (int i = 0; i < boxes.length; i++) {

            if (boxType == 0) {
                return false;
            }

            int checkX = -boxes[i].y + boxes[0].y + boxes[0].x;
            int checkY = boxes[i].x - boxes[0].x + boxes[0].y;
            if (borderJudgment(checkX, checkY)) {
                return false;
            }
        }
//        遍历每一个数组，每一个都绕着中心点顺时针选装90度，算法笛卡尔公式
        for (int i = 0; i < boxes.length; i++) {

            int checkX = -boxes[i].y + boxes[0].y + boxes[0].x;
            int checkY = boxes[i].x - boxes[0].x + boxes[0].y;
            boxes[i].x = checkX;
            boxes[i].y = checkY;
        }

        return true;
    }

    /**
     * 边界判断
     *
     * @param x
     * @param y
     * @return true出界false未出界
     */
    public boolean borderJudgment(int x, int y) {

        return (x < 0 || y < 0 || x >= mapsModel.maps.length || y >= mapsModel.maps[0].length || mapsModel.maps[x][y]);
    }

    /**
     * 上下左右移动
     *
     * @param i 上下
     * @param j 左右
     * @return 移动成功与否
     */
    public boolean move(int i, int j) {

        for (int k = 0; k < boxes.length; k++) {
            if (borderJudgment(boxes[k].x + i, boxes[k].y + j)) {
                return false;
            }
        }

        for (int k = 0; k < boxes.length; k++) {
            boxes[k].x += i;
            boxes[k].y += j;
        }

        return true;
    }


    /**
     * 绘制方块
     * @param canvas
     */
    public void drawBoxes(Canvas canvas) {

        for (int i = 0; i < boxes.length; i++) {
            canvas.drawRect(boxes[i].x * boxSize, boxes[i].y * boxSize, boxes[i].x * boxSize + boxSize, boxes[i].y * boxSize + boxSize, boxPaint);
        }

    }

    /**
     * 绘制下一块
     * @param canvas
     */
    public void drawNext(Canvas canvas) {
        boxesText = getNowBoxes();
        for (int i = 0; i < boxesText.length; i++) {
            canvas.drawRect(boxesText[i].x * 25, boxesText[i].y * 25, boxesText[i].x * 25 + 25, boxesText[i].y * 25 + 25, boxPaint);
        }
    }

    public Point[] getBoxes() {
        return boxes;
    }

    public void setBoxes(Point[] boxes) {
        this.boxes = boxes;
    }

    public int getxWright() {
        return xWright;
    }

    public void setxWright(int xWright) {
        this.xWright = xWright;
    }

    public int getxHeight() {
        return xHeight;
    }

    public void setxHeight(int xHeight) {
        this.xHeight = xHeight;
    }

    public int getBoxSize() {
        return boxSize;
    }

    public void setBoxSize(int boxSize) {
        this.boxSize = boxSize;
    }

    public int getBoxType() {
        return boxType;
    }

    public void setBoxType(int boxType) {
        this.boxType = boxType;
    }

    public Paint getBoxPaint() {
        return boxPaint;
    }

    public void setBoxPaint(Paint boxPaint) {
        this.boxPaint = boxPaint;
    }

    public MapsModel getMapsModel() {
        return mapsModel;
    }

    public void setMapsModel(MapsModel mapsModel) {
        this.mapsModel = mapsModel;
    }
}
